# APP-GCR-Geobarometer

Project to build and deploy a geobarometer web service built on a ThermoEngine runtime container image to Google Cloud Run.

- App and configuration Dockerfile in Geobarometer folder 

function test (GET protocol):  

http://0.0.0.0:8080?SiO2=77.5&TiO2=0.08&Al2O3=12.5&Fe2O3=0.207&Cr2O3=0.0&FeO=0.473&MnO=0.0&MgO=0.03&NiO=0.0&CoO=0.0&CaO=0.43&Na2O=3.98&K2O=4.88&P2O5=0.0&H2O=10.0&fO2%20offset=0.0

